package set2;

public class EasyECBEncryptor extends BlockEncryptor {

	public EasyECBEncryptor(byte[] secretText) {
		super(secretText);
	}

	@Override
	public byte[] encrypt(byte[] chosenText) {
		if (chosenText == null || chosenText.length == 0) {
			return set1.Set1Ciphers.aes128ECBEncrypt(secretText, globalKey);
		}

		byte[] plaintext = new byte[chosenText.length + secretText.length];
		System.arraycopy(chosenText, 0, plaintext, 0, chosenText.length);
		System.arraycopy(secretText, 0, plaintext, chosenText.length, secretText.length);

		return set1.Set1Ciphers.aes128ECBEncrypt(plaintext, globalKey);
	}

}
