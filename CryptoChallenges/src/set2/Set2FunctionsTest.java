package set2;

import static org.junit.Assert.*;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.Test;

public class Set2FunctionsTest {
	@Test
	public void testAes128CBCBlockAligned() {
		byte[] data = "TEST DATA 123456".getBytes();
		byte[] key = new byte[Set2Functions.AES128_KEYSIZE];
		byte[] IV = new byte[Set2Functions.AES_BLOCKSIZE];
		SecureRandom random = new SecureRandom();
		random.nextBytes(key);
		random.nextBytes(IV);
		byte[] encrypted = Set2Functions.aes128CBCEncrypt(data, key, IV);
		byte[] decrypted = Set2Functions.aes128CBCDecrypt(encrypted, key, IV);
		
		assertArrayEquals(data, decrypted);
	}
	
	@Test
	public void testAes128CBCUnaligned() {
		byte[] data = "123456789123456789123456789".getBytes();
		byte[] key = new byte[Set2Functions.AES128_KEYSIZE];
		byte[] IV = new byte[Set2Functions.AES_BLOCKSIZE];
		SecureRandom random = new SecureRandom();
		random.nextBytes(key);
		random.nextBytes(IV);
		byte[] encrypted = Set2Functions.aes128CBCEncrypt(data, key, IV);
		byte[] decrypted = Set2Functions.aes128CBCDecrypt(encrypted, key, IV);
		
		assertArrayEquals(data, decrypted);
	}
	
	@Test
	public void testAddRandBytes() {
		byte[] plaintext = "abc".getBytes();
		byte[] addedStuff;

		// Due to randomness, run it a few times
		for (int i = 0; i < 100; i++) {
			addedStuff = Set2Functions.addRandBytes(plaintext);
			//System.out.println(new String(addedStuff));
			assertTrue(addedStuff.length >= plaintext.length + 10);
			assertTrue(addedStuff.length <= plaintext.length + 20);
			assertTrue(new String(addedStuff).contains("abc"));
		}
	}
	
	@Test
	public void testGenerateAES128Key() {
		byte[] key = Set2Functions.generateAES128Key();
		//System.out.println(Set1Functions.bytesToHex(key));
		
		assertTrue(key.length == Set2Functions.AES128_KEYSIZE);
	}
	
	@Test
	public void testHashMap() {
		HashMap<String, String> map = new HashMap<String, String>();
		byte[] key = {(byte)0xAA, (byte)0xAB};
		byte[] value = {(byte)0x12};
		Set2Functions.putBytesInHashMap(map, key, value);
		assertTrue(Arrays.equals(value, Set2Functions.getBytesFromHashMap(map, key)));
	}

}
