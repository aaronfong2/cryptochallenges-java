package set2;

import java.util.Arrays;
import java.util.HashMap;

import set1.Set1Ciphers;

public class EasyECBDecryptor {
	BlockEncryptor enc;
	public EasyECBDecryptor(BlockEncryptor enc) {
		this.enc = enc;
	}

	public byte[] decrypt() throws Exception{
		byte[] secretText = new byte[enc.encrypt(null).length];
		int blocksize = findBlockSize();
		if (blocksize != Set2Functions.AES_BLOCKSIZE) {
			throw new Exception("This is not AES!");
		}
		if (!detectECB(blocksize)) {
			throw new Exception("This is not ECB!");
		}
		
		for (int i = 0; i < secretText.length; i++) {
			try {
				recoverByteN(secretText, i, blocksize);
			}
			catch (Exception e) {
				// If byte can't be recovered, maybe it's a padding thing...
				// Just return what we have so far
				break;
			}
		}
		
		return secretText;
	}
	
	private int findBlockSize() {
		int i = 1;
		byte[] filler;
		byte[] unpaddedCryptotext = enc.encrypt(null);
		byte[] cryptotext;
		byte[] temp;
		
		// Arbitrary limit
		while (i < 9999) {
			filler = new byte[i];
			Arrays.fill(filler, (byte)'A');
			cryptotext = enc.encrypt(filler);
			temp = Arrays.copyOfRange(cryptotext, i, cryptotext.length);

			if (Arrays.equals(unpaddedCryptotext, temp)) {
				return i;
			}
			i++;
		}
		
		return -1;
	}
	
	private boolean detectECB(int blocksize) {
		byte[] filler = new byte[3*blocksize];
		Arrays.fill(filler, (byte)'A');
		return Set1Ciphers.detectAES128ECB(enc.encrypt(filler));
	}
	
	/**
	 * Recover byte desiredByte. Requires bytes [0..desiredByte-1] to be known.
	 * @param currentlyKnownText
	 * @param desiredByte
	 * @param blocksize
	 */
	private void recoverByteN(byte[] currentlyKnownText, int desiredByte, int blocksize) throws Exception {
		HashMap<String, String> map = new HashMap<String,String>(256);
		int blockNumber = desiredByte / blocksize;
		int blockOffset = desiredByte % blocksize;
		byte[] filler = new byte[blocksize * (blockNumber + 1)];
		byte[] cryptotext;
		
		Arrays.fill(filler, 0, blocksize - blockOffset - 1, (byte)'A');
		if (desiredByte > 0) {
			System.arraycopy(currentlyKnownText, 0, filler, blocksize - desiredByte - 1, desiredByte);
		}
		for (int i = 0; i < 256; i++) {
			filler[blocksize - 1] = (byte)i;
			cryptotext = enc.encrypt(filler);
			// Only put the first block into the HashMap
			Set2Functions.putBytesInHashMap(map, Arrays.copyOf(cryptotext, blocksize), filler);
		}
		
		filler = new byte[blocksize - desiredByte - 1];
		Arrays.fill(filler, (byte)'A');
		cryptotext = enc.encrypt(filler);
		byte[] correctFiller = Set2Functions.getBytesFromHashMap(map, Arrays.copyOf(cryptotext, blocksize));
		byte secretByte;
		if (correctFiller == null) {
			throw new Exception("Something went wrong during byte-at-a-time decryption");
		}
		else {
			secretByte = correctFiller[blocksize - 1];
		}
		currentlyKnownText[desiredByte] = secretByte;
	}
}