package set2;

import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;

import javax.crypto.BadPaddingException;

import set1.Set1Ciphers;
import set1.Set1Functions;

public class Set2Functions {
	// Blocksize in bytes
	public static final int AES_BLOCKSIZE = 16;
	public static final int AES128_KEYSIZE = 16;
	
	public static final int AES_MODE_ECB = 1;
	public static final int AES_MODE_CBC = 2;
	
	public static final Charset charset = Charset.forName("US-ASCII");
	
	public static byte[] PKCS7Pad(byte[] plaintext, int blocksize) {
		if (blocksize <= 0) {
			throw new IllegalArgumentException("Blocksize must be >0");
		}
		else if (blocksize >= 256) {
			throw new IllegalArgumentException("Blocksize must be <256");
		}

		// Use short to prevent overflow problems
		short numPadding = (short)(blocksize - plaintext.length % blocksize);
		if (numPadding != 0) {
			byte paddedPT[] = Arrays.copyOf(plaintext, plaintext.length + numPadding);
			for (int i = plaintext.length; i < paddedPT.length; i++) {
				paddedPT[i] = (byte)numPadding;
			}
			return paddedPT;
		}
		else {
			return Arrays.copyOf(plaintext, plaintext.length);
		}
	}
	
	public static byte[] PKCS7Unpad(byte[] plaintext) throws BadPaddingException {
		// Use short to avoid overflow problems
		short pad = (short)(plaintext[plaintext.length - 1] & 0xFF);
		if (pad <= 0) {
			throw new BadPaddingException();
		}
		// Validate padding
		for (int i = 1; i < pad; i++) {
			if (plaintext[plaintext.length - i - 1] != pad) {
				throw new BadPaddingException();
			}
		}
		return Arrays.copyOf(plaintext, plaintext.length - pad);
	}
	
	public static byte[] aes128CBCEncrypt(byte[] plaintext, byte[] key, byte[] IV) {
		byte[] prevBlock = null;
		byte[] currBlock = null;
		byte[] ptBlock = null;
		byte[] paddedPT = PKCS7Pad(plaintext, AES_BLOCKSIZE);
		byte[] encrypted = new byte[paddedPT.length];
		
		// This should probably throw some exception
		if (IV.length != AES_BLOCKSIZE) {
			return null;
		}
		
		prevBlock = Arrays.copyOf(IV, AES_BLOCKSIZE);
		for (int i = 0; i < paddedPT.length; i+=AES_BLOCKSIZE) {
			// XOR with previous block, then AES encrypt 1 block
			ptBlock = Arrays.copyOfRange(paddedPT, i, i+AES_BLOCKSIZE);
			currBlock = Set1Ciphers.aes128ECBEncryptNoPadding(Set1Functions.bytesXOR(ptBlock, prevBlock), key);
			System.arraycopy(currBlock, 0, encrypted, i, AES_BLOCKSIZE);
			prevBlock = currBlock;
		}

		return encrypted;
	}
	
	public static byte[] aes128CBCDecrypt(byte[] cryptotext, byte[] key, byte[] IV) {
		byte[] decrypted = new byte[cryptotext.length];
		byte[] currBlock = null;
		byte[] prevBlock = null;
		byte[] ptBlock = null;
		
		// This should probably throw some exception
		if (IV.length != AES_BLOCKSIZE || cryptotext.length % AES_BLOCKSIZE != 0) {
			return null;
		}
		
		prevBlock = Arrays.copyOf(IV, AES_BLOCKSIZE);
		for (int i = 0; i < cryptotext.length; i+=AES_BLOCKSIZE) {
			currBlock = Arrays.copyOfRange(cryptotext, i, i+AES_BLOCKSIZE);
			// AES decrypt 1 block, then XOR with previous block
			ptBlock = Set1Functions.bytesXOR(Set1Ciphers.aes128ECBDecryptNoPadding(currBlock, key), prevBlock);
			System.arraycopy(ptBlock, 0, decrypted, i, AES_BLOCKSIZE);
			prevBlock = currBlock;
		}
		
		try {
			return PKCS7Unpad(decrypted);
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static byte[] generateAES128Key() {
		byte[] key = new byte[AES128_KEYSIZE];
		SecureRandom random = new SecureRandom();
		
		random.nextBytes(key);
		return key;
	}
	
	public static byte[] generateAESIV() {
		return generateAES128Key();
	}
	
	/*
	 * Randomly add 5-10 bytes before and after given plaintext
	 */
	public static byte[] addRandBytes(byte[] plaintext) {
		SecureRandom random = new SecureRandom();
		int prepend = 5 + random.nextInt(6);
		int append = 5 + random.nextInt(6);
		byte[] prependBytes = new byte[prepend];
		byte[] appendBytes = new byte[append];
		byte[] randomStuffAdded = new byte[plaintext.length + prepend + append];
		
		random.nextBytes(prependBytes);
		random.nextBytes(appendBytes);
		
		System.arraycopy(prependBytes, 0, randomStuffAdded, 0, prepend);
		System.arraycopy(plaintext, 0, randomStuffAdded, prepend, plaintext.length);
		System.arraycopy(appendBytes, 0, randomStuffAdded, prepend+plaintext.length, append);
		
		return randomStuffAdded;
	}
	
	public static byte[] aes128ECBRandEncrypt(byte[] plaintext) {
		byte[] key = generateAES128Key();
		
		return Set1Ciphers.aes128ECBEncrypt(plaintext, key);
	}
	
	public static byte[] aes128CBCRandEncrypt(byte[] plaintext) {
		byte[] key = generateAES128Key();
		byte[] IV = generateAESIV();
		
		return aes128CBCEncrypt(plaintext, key, IV);
	}
	
	public static byte[] randAESModeEncrypt(byte[] plaintext) {
		byte[] randomStuffAdded = addRandBytes(plaintext);
		SecureRandom random = new SecureRandom();
		if (random.nextBoolean()) {
			System.out.println("Encrypted with ECB");
			return aes128ECBRandEncrypt(randomStuffAdded);
		}
		else {
			System.out.println("Encrypted with CBC");
			return aes128CBCRandEncrypt(randomStuffAdded);
		}
	}
	
	public static void cipherModeDetector(int loops) {
		// Message needs to be at least 3 blocks long
		byte[] plaintext = new byte[3*AES_BLOCKSIZE];
		Arrays.fill(plaintext, (byte)0xAA);
		byte[] encrypted;
		boolean ecbDetected;

		for (int i = 0; i < loops; i++) {
			encrypted = randAESModeEncrypt(plaintext);
			ecbDetected = Set1Ciphers.detectAES128ECB(encrypted);
			
			if (ecbDetected) {
				System.out.println("Detected ECB");
			}
			else {
				System.out.println("Detected CBC");
			}
		}
	}
	
	// Utility functions for adding and getting byte arrays from HashMap
	// by transforming them into Strings
	
	public static byte[] putBytesInHashMap(HashMap<String,String> map, byte[] key, byte[] value) {
		String previous = map.put(new String(key,charset), new String(value,charset));
		if (previous == null) {
			return null;
		}
		else {
			return previous.getBytes(charset);
		}
	}
	
	public static byte[] getBytesFromHashMap(HashMap<String,String> map, byte[] key) {
		String value = map.get(new String(key,charset));
		
		if (value == null) {
			return null;
		}
		else {
			return value.getBytes(charset);
		}
	}
}
