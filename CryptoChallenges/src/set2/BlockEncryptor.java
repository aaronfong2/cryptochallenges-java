package set2;

import java.util.Arrays;

/**
 * Abstract class that is an ECB encrypting oracle.
 * The encrypt function allows chosen plaintext to be concatenated with
 * the secretText in an implementation-dependent way.
 * @author aaron
 *
 */
public abstract class BlockEncryptor {
	protected byte[] globalKey; 
	protected byte[] secretText;
	
	public BlockEncryptor(byte[] secretText) {
		this.secretText = Arrays.copyOf(secretText, secretText.length);
		this.globalKey = Set2Functions.generateAES128Key();
	}

	public abstract byte[] encrypt(byte[] chosenText);
}
