package set2;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class EasyECBDecryptorTest {

	@Test
	public void testDecryptFirstBlock() {
		byte[] secretText = "superlongsecrettext".getBytes();
		byte[] oneBlockOfSecretText = Arrays.copyOf(
						Set2Functions.PKCS7Pad(secretText, Set2Functions.AES_BLOCKSIZE),
						Set2Functions.AES_BLOCKSIZE);
		EasyECBEncryptor enc = new EasyECBEncryptor(secretText);
		EasyECBDecryptor dec = new EasyECBDecryptor(enc);
		
		byte[] decrypted = null;
		try {
			decrypted = dec.decrypt();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(new String(secretText));
		//System.out.println(new String(decrypted));

		assertArrayEquals(oneBlockOfSecretText, Arrays.copyOf(decrypted, Set2Functions.AES_BLOCKSIZE));
	}
	
	@Test
	public void testDecryptPaddedFirstBlock() {
		byte[] secretText = "text".getBytes();
		int textLen = secretText.length;
		
		EasyECBEncryptor enc = new EasyECBEncryptor(secretText);
		EasyECBDecryptor dec = new EasyECBDecryptor(enc);
		
		byte[] decrypted = null;
		try {
			decrypted = dec.decrypt();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertArrayEquals(secretText, Arrays.copyOf(decrypted, textLen));
	}
	
	@Test
	public void testDecrypt() {
		byte[] secretText = "superlongsecrettext".getBytes();
		int textLen = secretText.length;
		EasyECBEncryptor enc = new EasyECBEncryptor(secretText);
		EasyECBDecryptor dec = new EasyECBDecryptor(enc);
		
		byte[] decrypted = null;
		
		try {
			decrypted = dec.decrypt();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		assertArrayEquals(secretText, Arrays.copyOf(decrypted, textLen));
		
	}
}
