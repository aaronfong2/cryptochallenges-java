package set1;

import set1.Set1Ciphers;
import set2.Set2Functions;

import java.security.SecureRandom;
import java.util.Arrays;

import static org.junit.Assert.*;


import org.junit.Test;

public class Set1CiphersTest {
	private static final int AES128_KEYSIZE = 16;
	private static final int AES_BLOCKSIZE = 16;

	@Test
	public void testAes128ECB() {
		byte data[] = "Hello World! SECRET ABCDEFGHIJKLMNOPQRSTUVWXYZ".getBytes();
		SecureRandom keygen = new SecureRandom();
		byte key[] = new byte[AES128_KEYSIZE];

		keygen.nextBytes(key);
		System.out.println(Set1Functions.bytesToHex(key));
		
		byte encoded[] = Set1Ciphers.aes128ECBEncrypt(data, key);
		System.out.println(Set1Functions.bytesToHex(encoded));
		byte decoded[] = Set1Ciphers.aes128ECBDecrypt(encoded, key);
		System.out.println(new String(decoded));
		assertArrayEquals(data, decoded);
		
	}
	
	@Test
	public void testDetectAes128ECB() {
		byte data[] = new byte[AES_BLOCKSIZE * 3];
		Arrays.fill(data, 7, data.length, (byte)0xAA);
		
		SecureRandom keygen = new SecureRandom();
		byte key[] = new byte[AES128_KEYSIZE];
		keygen.nextBytes(key);
		
		byte encrypted[] = Set1Ciphers.aes128ECBEncrypt(data, key);
		System.out.println(Set1Functions.bytesToHexInBlocks(encrypted, AES_BLOCKSIZE));
		
		assertTrue(Set1Ciphers.detectAES128ECB(encrypted));
	}
	
	@Test
	public void testDetectAes128ECBFalse() {
		byte data[] = new byte[AES_BLOCKSIZE * 3];
		Arrays.fill(data, 7, data.length, (byte)0xAA);
		
		SecureRandom keygen = new SecureRandom();
		byte key[] = new byte[AES128_KEYSIZE];
		byte IV[] = new byte[AES_BLOCKSIZE];
		keygen.nextBytes(key);
		
		byte encrypted[] = Set2Functions.aes128CBCEncrypt(data, key, IV);
		System.out.println(Set1Functions.bytesToHexInBlocks(encrypted, AES_BLOCKSIZE));
		
		assertFalse(Set1Ciphers.detectAES128ECB(encrypted));
		
	}

}
